package example.restapiexample;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("api/v1")
public class HelloWorldController {
    @GetMapping("hello")
    public HashMap<String, Object> hello(){

        HashMap<String, Object> data = new HashMap<>();
        data.put("name", "Udin");
        return data;
    }
}
