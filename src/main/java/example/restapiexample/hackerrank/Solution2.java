package example.restapiexample.hackerrank;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;



class Result2 {

    /*
     * Complete the 'findShortestSubstring' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts STRING s as parameter.
     */

    public static int findShortestSubstring(String s) {
        // Write your code here The Shortest Substring
        char[] str = s.toCharArray();
        int index = 0;
        int count = 0;

        for(int i = 0; i < s.length(); i++){

            int j;
            for(j = 0; j < i; j++){
                if(str[i] == str[j]){
                    count++;
                    break;
                }
            }
            if(j == i){
                str[index] = str[i];
                index++;
            }
        }
        return count;
    }

}

public class Solution2 {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = bufferedReader.readLine();

        int result = Result2.findShortestSubstring(s);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}

