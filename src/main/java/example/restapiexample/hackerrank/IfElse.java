package example.restapiexample.hackerrank;

import java.util.Scanner;

public class IfElse {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String value = "";

        if(n % 2 == 1){
            value="Weird";
        }else{
            if(n >= 2 && n <= 20){
                value="Weird";
            }
            else{
                value="Not Weird";
            }
        }
        System.out.println(value);
    }
}
