package example.restapiexample.hackerrank;

import java.util.Scanner;

public class SdInSdOut {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        int c = scan.nextInt();

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);

        //second stIn & stOut
        Scanner scan1= new Scanner(System.in);
        int i = scan1.nextInt();
        Double d = scan1.nextDouble();
        scan1.nextLine();
        String s = scan1.nextLine();


        // Write your code here.

        System.out.println("String: " + s);
        System.out.println("Double: " + d);
        System.out.println("Int: " + i);
    }
}

